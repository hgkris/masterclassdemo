<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 20-5-14
 * Time: 13:33
 */

class PostSentEmailObserver {

    public static function boot()
    {
        parent::boot();


    }

    public function saved(Post $model)
    {
        Mail::send('posts.saved_email', $model, function($message)
        {
            $message->to('harry@studyx.be', 'Harry van der Valk')->subject('Post has been saved!');
        });
    }
} 