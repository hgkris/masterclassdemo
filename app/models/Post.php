<?php

class Post extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'title'   => 'required',
    //    'content' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [
        'title',
        'content'
    ];

    public static function boot()
    {
        parent::boot();

        // Setup event bindings...
        Post::observe(new PostObserver);
        Post::observe(new PostSentEmailObserver);
        // Post::observe(new PostSendPostcardObserver);
    }

    /**
     * @return mixed
     */
    public function user() {
        return $this->belongsTo('User');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function getCommentCount()
    {
        return count($this->comments);
    }

//    function __toString()
//    {
//        return $this->title;
//    }
}