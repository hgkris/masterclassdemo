<?php
class ImageHelper extends TripleHelper {

    private $imageSizes;
    private $imageClasses;
    private $imageDerivatives;
    private $imageDerivativesReversed;
    private $fixedSizeImageDerivatives;

    public $uploadError;
    public $lastImageDimensions;

    public function __construct()
    {
        global $imageSizes, $imageClasses, $imageDerivatives, $imageDerivativesReversed, $fixedSizeImageDerivatives;

        $this->imageSizes = $imageSizes;
        $this->imageClasses = $imageClasses;
        $this->imageDerivatives = $imageDerivatives;
        $this->imageDerivativesReversed = $imageDerivativesReversed;
        $this->fixedSizeImageDerivatives = $fixedSizeImageDerivatives;
    }



    public function isValidImageUpload( $name, $imageClass )
    {
        global $allowedImageExtensions;
        $this->uploadError = '';
        if( !isset($_FILES[$name]) )
        {
            $this->uploadError = Lang::get('ImageHelper.uploadfilenotfound');
            return false;
        }
        $upload = $_FILES[$name];

        if( $upload['error'] != UPLOAD_ERR_OK)
        {
            $this->uploadError = Lang::get('ImageHelper.errorupload');
            return false;
        } else if( $upload['size'] == 0 ) {
            $this->uploadError = Lang::get('ImageHelper.errorfilesizenull');
        }

        $extension = strtolower(pathinfo($upload['name'], PATHINFO_EXTENSION));
        if( !in_array($extension, $allowedImageExtensions) )
        {
            $this->uploadError = 'Het bestand had geen geldige afbeelding extensie. Alleen de extensies ' . join(', ', $allowedImageExtensions) . ' worden geaccepteerd.';
            return false;
        }

        //check the width and height
        if( !isset($this->imageClasses[$imageClass]) && $imageClass != self::IMAGECLASS_ASIS ) //as-is is not part of the $imageClasses file, because it has no preconfigured dimensions
        {
            $this->uploadError = Lang::get('ImageHelper.interalerror');
            return false;
        }

        if( $imageClass != self::IMAGECLASS_ASIS )
        {
            $dimensions = $this->imageClasses[$imageClass];
            $imageInfo = getimagesize($upload['tmp_name']);
            if( $imageInfo === false || $imageInfo[0] == 0 || $imageInfo[1] == 0)
            {
                $this->uploadError = 'De afmetingen van de afbeelding konden niet worden bepaald. Is het een geldige afbeelding?';
                return false;
            }
            $imageDimensions = array($imageInfo[0], $imageInfo[1]);

            if( $dimensions[0] != 0 && $imageDimensions[0] < $dimensions[0] )
            {
                $this->uploadError = 'De lengte van de geuploade afbeelding is kleiner dan de minimum lengte                 <br>' .
                    $_FILES[$name]['name'] . ' lengte: ' . $imageDimensions[0] . ' hoogte: ' . $imageDimensions[1] .  '<br>' .
                    'minimum afmetingen  lengte: ' . $dimensions[0] . ' hoogte:' . $dimensions[1];
                return false;
            }
            if( $dimensions[1] != 0 && $imageDimensions[1] < $dimensions[1] )
            {
                $this->uploadError = 'De hoogte van de geuploade afbeelding is kleiner dan de minimum hoogte                 <br>' .
                    $_FILES[$name]['name'] . ' lengte: ' . $imageDimensions[0] . ' hoogte: ' . $imageDimensions[1] .  '<br>' .
                    'minimum afmetingen  lengte: ' . $dimensions[0] . ' hoogte:' . $dimensions[1];

                return false;
            }
        }
        return true;
    }

    public function getUrlForResizedImage( $imageLocation, $imageClass, $size = null )
    {
        if( $imageClass == ImageHelper::IMAGECLASS_ASIS )
            return LOCAL_FILE_UPLOAD_HTTP_PREFIX . $imageLocation;

        if(!isset($this->imageClasses[$imageClass]) && !isset($this->imageDerivativesReversed[$imageClass]) )
            return false;
        if( !isset($this->imageSizes[$size]) || ($imageClass !== self::IMAGECLASS_ASIS && $size === null) )
            return '';

        if( $imageClass == self::IMAGECLASS_ASIS )
        {

        } else if( strpos($imageLocation, '[size]') === false )
        {
            //this is not a resized image, this is from before this class
            $site = ON_HTTPS ? SITE_URL_SECURE : SITE_URL;
            $imageLocation = substr($imageLocation, 0, 1) == '/' ? substr($imageLocation, 1) : $imageLocation;
            return $site . '/' . (substr(CMS_IMAGE_DIR, count(CMS_IMAGE_DIR)-1, 1) == '/' ? substr(CMS_IMAGE_DIR,0, -1) : CMS_IMAGE_DIR ) . '/' . $imageLocation;
        }

        //check if it is a derivative, this required a different url building method
        if( isset($this->imageDerivativesReversed[$imageClass]) )
        {
            return $this->getUrlForResizedImageDerivative( $imageLocation, $imageClass, $size );
        }

        $sizedImageLocation = str_replace('[size]', $size, $imageLocation);

        $this->lastImageDimensions = $this->getImageDimensions($sizedImageLocation);

        return LOCAL_FILE_UPLOAD_HTTP_PREFIX . $sizedImageLocation;
    }


    private function getUrlForResizedImageDerivative( $imageLocation, $imageClass, $size )
    {
        $sizedImageLocation = str_replace('[size]', $imageClass . '-' . $size, $imageLocation);
        $this->lastImageDimensions = $this->getImageDimensions($sizedImageLocation);

        return LOCAL_FILE_UPLOAD_HTTP_PREFIX . $sizedImageLocation;
    }

    public function createResizedImages($sourceImage, $imageClass, $extension = null){
        if( $extension == NULL )
            $extension = pathinfo($sourceImage, PATHINFO_EXTENSION);

        $timepart = time();
        $hashpart = substr(sha1($sourceImage), 0, 12);

        if( $imageClass == self::IMAGECLASS_ASIS )
        {
            $baseFilename = $timepart . '_asis_' . $hashpart . '.' . $extension;
        } else {
            $baseFilename = $timepart . '_[size]_' . $hashpart . '.' . $extension;
        }

        return $this->createResizedImagesWithBaseFilename($sourceImage, $imageClass, $baseFilename);
    }

    public function createResizedImagesFromUrl( $url, $imageClass, $extension )
    {
        //store the image locally
        $fp = @fopen($url, 'r');
        if( $fp === false )
        {
            $this->uploadError = 'The source url ' . $url . ' could not be opened for reading.';
            return false;
        }

        $localImage = LOCAL_FILE_UPLOAD_DIR . '20120917_' . sha1($url) . '.' . $extension; //assume the images are always jpg
        $fpOut = @fopen($localImage, 'w');
        if( $fpOut === false)
        {
            $this->uploadError = 'The temporary location ' . $localImage . ' could not be opened for writing, for converting a online image to a local image.';
            return false;
        }

        while( !feof($fp) )
        {
            fwrite($fpOut, fread($fp, 2048));
        }

        fclose($fp);
        fclose($fpOut);

        $resizedImageLocation = $this->createResizedImages($localImage, $imageClass, $extension); //returned value can be false or a string

        //remove the local temp file
        @unlink($localImage);

        return $resizedImageLocation;
    }

    protected function createResizedImagesWithBaseFilename($sourceImage, $imageClass, $baseFilename){

        if( $imageClass != self::IMAGECLASS_ASIS && (!isset($this->imageClasses[$imageClass]) || empty($this->imageClasses[$imageClass]) ) ) {
            return false;
        }

        $extension = pathinfo($baseFilename, PATHINFO_EXTENSION);

        $datepart = date('Ym');
        $subFolder = $imageClass . '/' . $datepart . '/';

        $baseLocation = $subFolder . $baseFilename;

        $baseFolder = LOCAL_FILE_UPLOAD_DIR;

        if(!file_exists($baseFolder . $subFolder))
            mkdir($baseFolder . $subFolder , 0777, true);

        $baseLocation = $baseFolder . $subFolder . $baseFilename;

        //store original
        if( $imageClass == self::IMAGECLASS_ASIS )
        {
            copy($sourceImage, $baseLocation); //only copy this file
            return $subFolder . $baseFilename;
        } else {
            $target = str_replace('[size]', 'original', $baseLocation);
            copy($sourceImage, $target);
        }

        $targetBaseDimensions = $this->imageClasses[$imageClass];

        //store resized versions
        foreach ($this->imageSizes as $size => $fraction) {
            $target = str_replace('[size]', $size, $baseLocation);
            $width = $targetBaseDimensions[0] * $fraction;
            $height = $targetBaseDimensions[1] * $fraction;

            $this->resizeFileToLocal($sourceImage, $target, $width, $height);
        }

        //check if there are derivatives
        if( isset($this->imageDerivatives[$imageClass]) )
        {
            foreach ( $this->imageDerivatives[$imageClass] as $derivativeName => $derivativeDimensions) {
                foreach( $this->imageSizes as $size => $fraction )
                {
                    $target = str_replace('[size]', $derivativeName . '-' . $size, $baseLocation);
                    $width = $derivativeDimensions[0] * $fraction;
                    $height = $derivativeDimensions[1] * $fraction;

                    $this->resizeFileToLocal($sourceImage, $target, $width, $height);
                }
            }
        }

        if( isset($this->fixedSizeImageDerivatives[$imageClass]) ) {
            foreach ( $this->fixedSizeImageDerivatives[$imageClass] as $derivativeName => $derivativeDimensions) {
                $target = str_replace('[size]', $derivativeName . '-xlarge', $baseLocation);
                $width = $derivativeDimensions[0];
                $height = $derivativeDimensions[1];

                $this->resizeFileToLocal($sourceImage, $target, $width, $height);
            }
        }

        return $subFolder . $baseFilename;
    }

    public function deleteResizedImages($subFoldersAndFilenameBase, $imageClass)
    {
        if( $imageClass == self::IMAGECLASS_ASIS )
        {
            @unlink(LOCAL_FILE_UPLOAD_DIR . $subFoldersAndFilenameBase); //the filename is already the name of the one and only image
        } else {
            if(!isset($this->imageClasses[$imageClass]) || empty($this->imageClasses[$imageClass])){
                return;
            }

            $imageSizes = $this->imageSizes;
            $imageSizes['original'] = array(0,0);

            foreach ($imageSizes as $size => $fraction) {
                $subFoldersAndFilename = str_replace('[size]', $size, $subFoldersAndFilenameBase);

                unlink(LOCAL_FILE_UPLOAD_DIR . $subFoldersAndFilename);
            }

            if( isset($this->imageDerivatives[$imageClass]) )
            {
                foreach ( $this->imageDerivatives[$imageClass] as $derivativeName => $dimensions) {
                    $subFoldersAndFilename = str_replace('[size]', $derivativeName . '-' . $size, $subFoldersAndFilenameBase);
                    @unlink(LOCAL_FILE_UPLOAD_DIR . $subFoldersAndFilename);
                }
            }
        }
    }

    private function resizeFileToLocal($sourceImage, $target, $maxWidth, $maxHeight){
        $image = PhpThumbFactory::create($sourceImage);
        $image->resize($maxWidth, $maxHeight);

        try {
            $image->save($target);
            return true;
        }
        catch( Exception $e )
        {
            //there is no way to log, since there is no $app available
            return false;
        }
    }

    //TODO implement some caching here
    private function getImageDimensions( $imageLocation )
    {
        $localImageLocation = LOCAL_FILE_UPLOAD_DIR . $imageLocation;

        $imageInfo = @getimagesize($localImageLocation);
        if( $imageInfo === false )
            return array(0,0);

        return array($imageInfo[0], $imageInfo[1]);
    }
}
?>