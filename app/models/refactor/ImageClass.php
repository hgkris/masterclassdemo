<?php
/**
 * Created by PhpStorm.
 * User: kpro311
 * Date: 20-5-14
 * Time: 15:19
 */

class ImageClass {
    const FULLWIDTH = 'fullwidth';
    const VIDEOTHUMB = 'videothumb';
    const CARROUSELBANNER = 'carrouselbanner';
    const CARROUSELPRODUCT = 'carrouselproduct';
    const EDITORIALICON = 'editorialicon';
    const APPICON = 'appicon';
    const PRODUCTIMAGE = 'productimage';
    const PRICEPLANICON = 'priceplanicon';
    const PRICEPLANICONSMALL = 'priceplaniconsmall';
    const PRICEPLANICONND8 = 'nd8priceplanicon';
    const PRICEPLANICONND8SMALL = 'nd8priceplaniconsmall';
    const COUNTRYFLAG = 'countryflag';
    const COUNTRYFLAG_ROUND = 'countryroundflag';
    const ZONEIMAGE = 'zoneimage';
    const FEATURELARGE = 'mainfeature';
    const TOASTER = 'toaster';
    const ASIS = 'as-is'; //a special type of class where the image is simply uploaded, no resizing
    const WORLDELEMENT = 'worldelement';


    // For email (mCommerce)
    const PRICEPLANICONMAIL = 'priceplaniconmail';
    const PRICEPLANICONND8MAIL = 'nd8priceplaniconmail';
    const PRODUCTIMAGEMAIL = 'productimagemail';

    const IMAGESIZE_SMALL = 'small';
    const IMAGESIZE_MEDIUM = 'medium';
    const IMAGESIZE_LARGE = 'large';
    const IMAGESIZE_XLARGE = 'xlarge';
} 