<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 3-5-14
 * Time: 15:34
 */

class Role {

    const ROLE_ADMIN = 'admin';
    const ROLE_GUEST = 'guest';

    //const NO_ROOT = false;
    const ROOT = true;
} 