<?php

class DemoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $posts = Post::all();
        return View::make('post.index', compact('posts'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('post.create');
	}

    /**
     *
     */
    public function handleCreate()
    {

        $rules = array('title' => 'required', 'content' => 'required');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route('post.create')->withErrors($validator);
        }
        $post = AppFactory::createPost(Input::get('title'), Input::get('content'));
        $post->save();
        $event = Event::fire('post.created', array($post));
        Session::flash('message', 'Successfully created the post!');
        return Redirect::route('posts');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  Post  $post
	 * @return Response
	 */
	public function show($post)
	{
        return View::make('post.show', compact('post'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  Post $post
	 * @return Response
	 */
	public function edit($post)
	{
        return View::make('post.edit', compact('post'));
	}

    /**
     * @param  Post  $post
     */
    public function handleEdit($post)
    {
        $rules = array('title' => 'required', 'content' => 'required');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::route('post.edit')->withErrors($validator);
        }

        $post->title = Input::get('title');
        $post->content = Input::get('content');
        $post->save();
        $event = Event::fire('post.edited', array($post));
        Session::flash('message', 'Successfully updated the post!');
        return Redirect::route('posts');
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Post $post
	 * @return Response
	 */
	public function update($post)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Post $post
	 * @return Response
	 */
	public function delete($post)
	{
        return View::make('post.delete', compact('post'));
	}

    /**
     * @param  Post  $post
     */
    public function handleDelete($post)
    {
        Event::fire('post.deleted', array($post));
        $post->delete();
        Session::flash('message', 'Successfully deleted the post!');
        return Redirect::route('posts');
    }
}
