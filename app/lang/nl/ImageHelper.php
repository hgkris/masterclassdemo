<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 19-5-14
 * Time: 22:35
 */
return array (
    'uploadfilenotfound' => 'Het geuploade bestand kon niet worden gevonden.',
    'errorupload'  => 'Er is een fout opgetreden tijdens het uploaden van het bestand.',
    'errorfilesizenull' => 'Het geuploade bestand had een grootte van 0 bytes.',
    'interalerror' => 'Interne error, het type upload werd niet herkend.'
);
